

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		
		
		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");
		
		int guess = reader.nextInt();
		int attempt = 0 ;

		while (guess != number && guess != -1) {
			
			attempt = attempt + 1 ;

			System.out.println("Sorry Wrong Answer") ;
			if (guess > number){
				System.out.println("Mine is less than your guess " ) ;
			}
			else if ( guess < number ){
				System.out.println("Mine is greater than your guess " ) ;
			}			
			
			System.out.println("Type -1 or guess another number :");
			guess = reader.nextInt() ;
		}
		if ( number == guess ) {
			System.out.println("Congratulations you won after " + attempt + " attempts ") ;
		}
		else {
			System.out.println("Sorry the number was :" + number) ;
			System.out.println("You give up after " + attempt + " attempts " ) ;
		}
		
		
		reader.close(); //Close the resource before exiting
	}
	
	
}
