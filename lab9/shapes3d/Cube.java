package shapes3d;
import shapes2d.Square;


public class Cube extends Square{
    
    public Cube(int side) {
        super(side);
    }
    
    public int Area(){
        return 6*super.Area();
    }
    public int Volume(){
        return side*side*side ;
    }

    
}
