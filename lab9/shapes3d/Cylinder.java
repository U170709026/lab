package shapes3d;

import shapes2d.Circle;

public class Cylinder extends Circle{
    
    int height ;
    public Cylinder(int radius , int height) {
        super(radius);
        this.height = height ;
    }
    public double Volume(){
        return height * super.Area() ;
        
    }
    public double Area(){
        return super.Area()*2+2*Math.PI*radius*height ;
        
    }
}
