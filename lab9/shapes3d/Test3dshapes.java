
package shapes3d;

public class Test3dshapes {
    
    public static void main(String[] args) {
        Cube cube = new Cube(5);
        System.out.println(cube.Area());
        System.out.println(cube);
        Cylinder cylinder = new Cylinder(3, 6) ;
        System.out.println(cylinder.Area());
        System.out.println(cylinder);
    }
    
    
}
