
package shapes2d;




public class Circle {
    
    public int radius ;
    
    public Circle(int radius) {
        this.radius = radius ;
    }
    
    public double Area() {
        double area = Math.PI * radius * radius ;
        return area;
    }
    public String toString() {
		return "radius = " +radius;
	}
}
