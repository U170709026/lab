
package shapes2d;
import java.lang.Object;

public class Square extends Object{
    
    public int side ;
    
    public Square(int side) {
        this.side = side ;
    }
    public int Area(){
        int area = side * side ;
        return area ;
    }
    public String toString() {
		return "side = " + side;
	}
}
